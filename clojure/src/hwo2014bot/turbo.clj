(ns hwo2014bot.turbo
  (:require [hwo2014bot.predict-stream :as ps]
            [hwo2014bot.plan :as pl]))

(defn turbo-just-got-available [m]
  (let [avail? (get-in m [:cars :me 0 :turbo :available :turboDurationTicks])]
    (and avail? (not= avail? (get-in m [:cars :me 1 :turbo :available :turboDurationTicks])))))

(defn throttle-changed-to-one [m]
  (and (= 1.0 (get-in m [:cars :me 0 :throttle]))
       (= 0.0 (get-in m [:cars :me 1 :throttle]))))

(defn turbo-is-available [m]
  (get-in m [:cars :me 0 :turbo :available :turboDurationTicks]))

(defn throttle-is-one [m]
  (= 1.0 (get-in m [:cars :me 0 :throttle])))
       
(defn check-turbo? [m]
  (and (throttle-is-one m)
       (turbo-is-available m)))

(defn turbo-efficiency [m max-steps]
  (let [turbo-ticks (get-in m [:cars :me 0 :turbo :available :turboDurationTicks])
        one-count (count (take-while #(= % {:throttle 1.0})
                                     (drop 1 (pl/turbo-plan m max-steps max-steps 1))))]
    (if (and turbo-ticks (not (zero? turbo-ticks)))
      (assoc-in m [:turbo :efficiency]
                (/ one-count turbo-ticks))
      (assoc-in m [:turbo :efficiency] 0))))

#_(defn turbo-efficiency [m max-steps]
   (let [track (get-in m [:race :track])
         physic (get-in m [:race :physic])
         d-1 (first (get-in m [:cars :me]))
         d-2 (second (get-in m [:cars :me]))
         turbo-ticks (get-in d-1 [:turbo :available :turboDurationTicks])]
     (if (and d-1 d-2 turbo-ticks (not (zero? turbo-ticks)))
       (assoc-in m [:turbo :efficiency] (/ (ps/max-valid-one-log track physic (ps/one-predict track physic d-2 d-1 max-steps {:turbo "pewpew"}))
                                           turbo-ticks))
       (assoc-in m [:turbo :efficiency] 0))))

#_(defn max-turbo-efficiency [m]
   (let [me (get-in m [:race :plan :turbo :max-efficiency] 0)
         e (get-in m [:race :plan :turbo :efficiency] 0)]
     (if (< me e 1) ;more than one is not usefull.
       (assoc-in m [:race :plan :turbo :max-efficiency] e)
       m)))

#_(defn dim-max-turbo-efficiency [m rate]
   (let [me (get-in m [:race :plan :turbo :max-efficiency] 0)
         e (get-in m [:race :plan :turbo :efficiency] 0)]
     (if (< 0 e) ;we only dim if we could turbo here 
       (update-in m [:race :plan :turbo :max-efficiency] #(* % rate))
       m)))

(defn request-turbo [m min-eff]
   (let [e (get-in m [:turbo :efficiency] 0)]
     (if (<= min-eff e)
       (assoc-in m [:request :turbo] true)
       m)))