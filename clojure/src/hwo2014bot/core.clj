(ns hwo2014bot.core
  (:require [clojure.data.json :as json]
            [clojure.pprint :as pp]
            [hwo2014bot.handle :refer [handle-msg]])
  (:use [aleph.tcp :only [tcp-client]]
        [lamina.core :only [enqueue wait-for-result wait-for-message close closed?]]
        [gloss.core :only [string]])
  (:gen-class))

(defn- json->clj [string]
  (json/read-str string :key-fn keyword))

(defn send-message [channel message]
  (enqueue channel (json/write-str message)))

(defn read-message [channel]
  (json->clj
    (try
      (let [result (wait-for-message channel)]
        (if (= result "{}")
          (do (close channel)
            result)
          result))
      (catch Exception e
        (println (str "ERROR: " (.getMessage e)))
        #_(System/exit 1)))))

(defn connect-client-channel [host port]
  (wait-for-result
   (tcp-client {:host host,
                :port port,
                :frame (string :utf-8 :delimiters ["\n"])})))

(defn log-msg [msg]
  (case (:msgType msg)
    "join" (println "Joined")
    "gameStart" (println "Race started")
    "gameEnd" (println "Race ended")
    "error" (println (str "ERROR: " (:data msg)))
    :noop))

(def all-data (atom {}))

(defn game-loop [channel race-info cars-info version data-atom]
  (if (closed? channel)
    (println "Channel closed")
    (let [msg (try (read-message channel)
                (catch Exception e
                  {:msgType "empty"}))
          m {:race race-info :input msg :cars cars-info}
          id (:id race-info)
          result (try (handle-msg m)
                   (catch Exception e
                     #_(write-data (str "error-" id (if version "-") version ".edn")
                                  m)
                     (swap! data-atom conj m)
                     (throw e)
                     {:race  race-info
                      :cars  cars-info}))]
      (log-msg msg)
      #_(swap! data-atom conj result)
      (if (:answer result) (send-message channel (:answer result)))
      (recur channel (:race result) (:cars result) version data-atom))))


(defn -main[& [host port botname botkey trackname]]
  (let [channel (connect-client-channel host (Integer/parseInt port))]
    (reset! all-data [])
    (if trackname
      (send-message channel {:msgType "createRace" :data {:botId {:name botname :key botkey}
                                                          :trackName trackname
                                                          :carCount 1}})
      (send-message channel {:msgType "join" :data {:name botname :key botkey}}))
    (game-loop channel {} {}  nil all-data)))


(defn create-race [host botname botkey trackname number pw]
  (let [channel (connect-client-channel host (Integer/parseInt "8091"))]
    (reset! all-data [])
    (send-message channel {:msgType "createRace" :data {:botId {:name botname :key botkey}
                                                          :trackName trackname
                                                        :password pw
                                                          :carCount number}})
    (game-loop channel {} {} nil all-data)))

(defn start-race [trackname count]
  (let [t1 (Thread. (partial create-race "senna.helloworldopen.com" "Drowsy" "1HxjEOMo0Nx+1Q" trackname count "testpw"))]
    (.start t1)
  t1))

(defn join-race [host botname botkey opts]
    (let [channel (connect-client-channel host (Integer/parseInt "8091"))]
    (reset! all-data [])
      (send-message channel {:msgType "joinRace" :data (merge {:botId {:name botname :key botkey}}
                                                               opts)})
    (game-loop channel {} {} nil all-data)))

(defn start-joined-race [trackname count]
  (let [t1 (Thread. (partial join-race "testserver.helloworldopen.com" "Drowsy" "1HxjEOMo0Nx+1Q" {:carCount count}))]
    (.start t1)
  t1))

#_(def t (start-joined-race "keimola" 4))

#_(create-race "senna.helloworldopen.com" "Drowsy" "1HxjEOMo0Nx+1Q" "keimola" 4 "testpw")
#_(def t (start-race "elaeintarha" 4))
#_(.stop t)

